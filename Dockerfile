FROM ubuntu:22.04

USER root

# Установка системных пакетов
RUN apt-get update -y
RUN apt-get install -y build-essential zlib1g-dev lzma-dev libssl-dev libpq-dev libffi-dev
RUN apt-get install -y wget curl git sudo

# Добавление пользователя
ENV USER_NAME=mlops_user
RUN useradd -m -G users -s /bin/bash ${USER_NAME}
RUN echo "${USER_NAME} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
USER ${USER_NAME}
ENV HOME_PATH=/home/${USER_NAME}

# Установка pyenv и python
ENV PYTHON_VERSION=3.11.8
RUN curl http://pyenv.run | bash && \
	echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc && \
	echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc && \
	echo 'eval "$(pyenv init -)"' >> ~/.bashrc
ENV PYENV_PATH=${HOME_PATH}/.pyenv/bin/pyenv
RUN ${PYENV_PATH} install ${PYTHON_VERSION} && \
    ${PYENV_PATH} global ${PYTHON_VERSION}
ENV PYTHON_PATH=/home/mlops_user/.pyenv/shims/python

# Установка poetry
RUN curl -sSL http://install.python-poetry.org | ${PYTHON_PATH} - && \
	echo 'export PATH="$HOME/.local/bin:$PATH"' >> ~/.bashrc
ENV POETRY_PATH=${HOME_PATH}/.local/bin/poetry

# Установка проекта и окружения
ENV APP_DIR=${HOME_PATH}/app
COPY . ${APP_DIR}
RUN sudo chown ${USER_NAME} ${APP_DIR}
WORKDIR ${APP_DIR}
RUN ${POETRY_PATH} install

# В будущем тут будет код для запуска скриптов
