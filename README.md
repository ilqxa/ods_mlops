ods_mlops
==============================

My project for ODS MLops course v3

Workflow
------------
The repository is maintained using the [github flow](https://docs.github.com/en/get-started/using-github/github-flow) methodology.
Detailed instructions for creating a fork and committing changes to the repository can be found in `CONTRIBUTING.md`

Start up
------------
You can build image and run container following the next commands:
```bash
cd ods_mlops
docker build -t mlops_image .
docker run --rm -it --name mlops_container mlops_image
```
Used flags:
* `build -t` to set the tag an image
* `run --rm` to automatically remove the container when it exits
* `run -it` to keep STDIN open and allocate a pseudo-TTY
* `run --name` to assign a name to the container

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`.
    ├── Dockerfile         <- Dockerfile with command to build container.
    ├── README.md          <- The top-level README for developers using this project.
    ├── CONTRIBUTING.md    <- The instructions for project contributors.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── pyproject.toml   <- The requirements file for reproducing the analysis environment
    │
    └── src                <- Source code for use in this project.
        ├── __init__.py    <- Makes src a Python module
        │
        ├── data           <- Scripts to download or generate data
        │   └── make_dataset.py
        │
        ├── features       <- Scripts to turn raw data into features for modeling
        │   └── build_features.py
        │
        ├── models         <- Scripts to train models and then use trained models to make
        │   │                 predictions
        │   ├── predict_model.py
        │   └── train_model.py
        │
        └── visualization  <- Scripts to create exploratory and results oriented visualizations
            └── visualize.py


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
