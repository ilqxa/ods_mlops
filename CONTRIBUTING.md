How to contribute
=======

Clone repo
-------
Get your own repository copy
```bash
git clone https://gitlab.com/<OWNER>/<REPOSITORY>.git
cd <REPOSITORY>
```

Create a branch
-------
Create a branch in your repository. A short, descriptive branch name enables your collaborators to see ongoing work at a glance. For example, `increase-test-timeout` or `add-code-of-conduct`
```bash
git checkout -b new-feature-branch master
```

Setup the environment
--------
Install all you need with the help of [poetry](https://python-poetry.org/)
```bash
poetry install
```

Make changes
-------
On your branch, make any desired changes to the repository.

Your branch is a safe place to make changes. If you make a mistake, you can revert your changes or push additional changes to fix the mistake. Your changes will not end up on the default branch until you merge your branch.

Conduct the review
-------
Check your code style and fix the inconsistency
```bash
ruff format
ruff check
```

Commit and push your changes
-------
Give each commit a descriptive message to help you and future contributors understand what changes the commit contains. For example, `fix typo` or `increase rate limit`

```bash
git add my_files
git commit -m "I promise to replace this text"
git push origin new-feature-branch
```

Create merge request
-------
[Create a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-create-a-branch) to ask collaborators for feedback on your changes. MR review is so valuable that some repositories require an approving review before MRs can be merged. If you want early feedback or advice before you complete your changes, you can mark your MR as a draft.

Reviewers should leave questions, comments, and suggestions. Reviewers can comment on the whole pull request or add comments to specific lines or files. You and reviewers can insert images or code suggestions to clarify comments.

Merge your MR and delete the branch
-------
Once your MR is approved, merge your MR. After you do it, delete your branch.